#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys, StringIO, hashlib
#reload(sys)
#sys.setdefaultencoding('utf-8')

__author__ = "Filippo Baruffaldi"
__version__ = "0.0.3"

def md5_for_file(f, block_size=2**20):
    md5 = hashlib.md5()
    while True:
        data = f.read(block_size)
        if not data:
            break
        md5.update(data)
    return md5.digest()
   

if __name__ == "__main__":
	fd = open('movie.mp4', "rb")
	c = 0
	content = "".join([x for x in fd.readlines()])
	files = content.split('GET /ondemand/VOD_HD/replay_nogeo/emozioni-1506201223.40.00/emozioni-1506201223.40.000.ism/')
	for file in files[1:]:
		lines = []
		fdd = open('dst_'+str(c)+'.mp4', 'wb')
		c = c+1
		for line in file.split("\n"):
			if line.strip() != '' and line.strip() and line.strip().startswith(("QualityLevels(","<cross-","GET ","ETag:","<?xml","<allow-","<domain","</allow-","<access-","<grant-","<resource","</grant-","</policy","<policy","</cross-","</access-","HTTP","Pragma:","Content-","Last-","Server:","Cache-","Connection:","Date:","Accept","Host","User-","Referer","If-")):
				""" """
				#print line
				#pass
			else: lines.append(line)
		fdd.write("\n".join(lines).strip())
		fdd.close()
#	for line in fd.readlines():
#		if line.strip() != '' and line.strip() and line.strip().startswith(("<cross-","GET ","ETag:","<?xml","<allow-","<domain","</allow-","<access-","<grant-","<resource","</grant-","</policy","<policy","</cross-","</access-","HTTP","Pragma:","Content-","Last-","Server:","Cache-","Connection:","Date:","Accept","Host","User-","Referer","If-")):
#			print line
#			#pass
#		else: lines.append(line)
	#fdd.write("".join(lines))
	#fdd.close()
	fd.close()
	
print "done."